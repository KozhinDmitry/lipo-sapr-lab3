# Makefile using Lex to build


CC     = g++
CFLAGS = -c

LEX    = flex++
LFLAGS =

LN     = g++
LNFLAGS=

all:	calc

test: all
	@echo "-int test-"
	./calc --test < test1.txt
	@echo "-double test-"
	./calc --test < test_for_double.txt
	@echo "-array test-"
	./calc --test < test_for_array.txt

calc:	calc.c
	$(LN) $(LNFLAGS) -o calc calc.c

calc.o:	calc.c
	$(CC) $(CFLAGS) -o calc.o calc.c

calc.cpp: calc.l
	$(LEX) $(LFLAGS) -o calc.c calc.l

clean:
	rm -f calc calc.c *.o