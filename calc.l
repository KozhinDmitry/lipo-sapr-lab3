%{
/*
This file is part of Reverse Notation Calc.

    Reverse Notation Calc is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <iostream>
#include <sstream>
#include <stack>
#include <vector>
#include <string>

using namespace std;

#define CHECK_RET_MINUS_ONE(cond) if ((cond)) { return -1; }
#define debug(code) if (is_debug) { (code); }

typedef enum {
    LexNumber = 1001,
    LexArray,
    LexParseError,
    LexPlus,
    LexMinus,
    LexDiv,
    LexMult,
    LexEnd,
    LexUnknown,
} LexType;

typedef enum {
    DOUBLE,
    ARRAY
} stackType;

typedef vector<double> array;

stack<double> num_stack;
stack<array> arr_stack;

stack<stackType> type_stack;

bool end_file;
double double_value;

bool inside_array = false;
bool array_last_num = false;

array global_array;

string print_array(array v);

bool is_debug = false;

bool is_testing = false;

%}

%s SKIPERROR

digit     [0-9]+
dot       \.
number    ({digit}|{digit}{dot}{digit})

%%

{number}    {
    double_value = atof(yytext);
    if (inside_array) {
            global_array.push_back(double_value);
            array_last_num = true;
    } else {
        debug(cout << "found " << double_value << endl)
        return LexNumber; 
    }
}
\+          {
    debug(cout << "plus" << endl)
    return LexPlus;
}
\-          {
    debug(cout << "minus" << endl)
    return LexMinus;
}
\/          {
    debug(cout << "div" << endl)
    return LexDiv;
}
\*          {
    debug(cout << "mult" << endl)
    return LexMult; 
}
,\]         {

    return LexParseError;
}
,           {
    if (!inside_array || !array_last_num) {

        return LexParseError;
    } else {

        array_last_num = false;
    }
}
\[          {
    if (inside_array) {

        return LexParseError;
    } else {
        inside_array = true;
        array_last_num = false;
    }
}
\]          {

    if (!inside_array || !array_last_num) {
        return LexParseError;
    } else {
        debug(cout << "found an " << print_array(global_array) << endl)
        // flag reset
        inside_array = false;
        array_last_num = false;
        return LexArray;
    }
}
^[ \t]*\n   {
    if (is_testing) {
        cout << "empty line" << endl;
    } else {
        cout << "empty line, care to try again?" << endl << "> ";
    }
}
\n          {
    debug(cout << "CR" << endl)
    return LexEnd; 
}
[ \t]       {
}
.           {
    return LexUnknown;
}
<SKIPERROR>[^\n]* { debug(cout << "skipping '" << yytext << "'" << endl) }

%%

int process_command(int token)
{
    debug(cout << "token : " << token << endl)

    switch (token) 
    {
    case LexNumber:
        num_stack.push(double_value);
        type_stack.push(DOUBLE);
        break;
    case LexArray: 
    {
        if (global_array.size() > 0) 
        {
            array v;
            for (auto i = 0; i < global_array.size(); i++) 
            {
                v.push_back(global_array[i]);
            }
            arr_stack.push(v);
            type_stack.push(ARRAY);
            global_array.clear();
        }
        break; 
        }
    case LexPlus: 
    {
        CHECK_RET_MINUS_ONE(type_stack.empty())
        stackType right_type = type_stack.top();
        type_stack.pop();
        CHECK_RET_MINUS_ONE(type_stack.empty())
        stackType left_type = type_stack.top();
        type_stack.pop();

        if (left_type == DOUBLE && right_type == DOUBLE) 
        {
            CHECK_RET_MINUS_ONE(num_stack.empty())
            double a = num_stack.top();
            num_stack.pop();
            CHECK_RET_MINUS_ONE(num_stack.empty())
            double b = num_stack.top();
            num_stack.pop();

            num_stack.push(b + a);
            type_stack.push(DOUBLE);
        }

        if (left_type == ARRAY && right_type == ARRAY) 
        {
            CHECK_RET_MINUS_ONE(arr_stack.empty())
            array a = arr_stack.top();
            arr_stack.pop();
            CHECK_RET_MINUS_ONE(arr_stack.empty())
            array b = arr_stack.top();
            arr_stack.pop();
            array c;
            for (auto i = 0; i < b.size(); i++) 
            {
                c.push_back(b[i]);
            }
            for (auto i = 0; i < a.size(); i++) 
            {
                c.push_back(a[i]);
            }
            arr_stack.push(c);
            type_stack.push(ARRAY);
        }

        if (left_type == DOUBLE && right_type == ARRAY) 
        {
            CHECK_RET_MINUS_ONE(num_stack.empty())
            double a = num_stack.top();
            num_stack.pop();
            CHECK_RET_MINUS_ONE(arr_stack.empty())
            array b = arr_stack.top();
            arr_stack.pop();
            array c;
            c.push_back(a);
            for (auto i = 0; i < b.size(); i++) 
            {
                c.push_back(b[i]);
            }
            arr_stack.push(c);
            type_stack.push(ARRAY);
        }

        if (left_type == ARRAY && right_type == DOUBLE) 
        {
            CHECK_RET_MINUS_ONE(arr_stack.empty())
            array a = arr_stack.top();
            arr_stack.pop();
            CHECK_RET_MINUS_ONE(num_stack.empty())
            double b = num_stack.top();
            num_stack.pop();
            a.push_back(b);
            arr_stack.push(a);
            type_stack.push(ARRAY);
        }             

        break; 
    }
    case LexMinus: 
    {

        CHECK_RET_MINUS_ONE(type_stack.top() != DOUBLE || type_stack.empty())
        CHECK_RET_MINUS_ONE(num_stack.empty())
        double a = num_stack.top();
        num_stack.pop();
        type_stack.pop();
        CHECK_RET_MINUS_ONE(type_stack.top() != DOUBLE || type_stack.empty())
        CHECK_RET_MINUS_ONE(num_stack.empty())
        double b = num_stack.top();
        num_stack.pop();
        type_stack.pop();

        num_stack.push(b - a);
        type_stack.push(DOUBLE);

        break; 
    }
    case LexDiv: 
    {
        CHECK_RET_MINUS_ONE(type_stack.top() != DOUBLE || type_stack.empty())
        CHECK_RET_MINUS_ONE(num_stack.empty())
        double a = num_stack.top();
        num_stack.pop();
        type_stack.pop();
        CHECK_RET_MINUS_ONE(type_stack.top() != DOUBLE || type_stack.empty())
        CHECK_RET_MINUS_ONE(num_stack.empty())
        double b = num_stack.top();
        num_stack.pop();
        type_stack.pop();

        num_stack.push(b / a);
        type_stack.push(DOUBLE);

        break; 
    }
    case LexMult: 
    {
        CHECK_RET_MINUS_ONE(type_stack.top() != DOUBLE || type_stack.empty())
        CHECK_RET_MINUS_ONE(num_stack.empty())
        double a = num_stack.top();
        num_stack.pop();
        type_stack.pop();
        CHECK_RET_MINUS_ONE(type_stack.top() != DOUBLE || type_stack.empty())
        CHECK_RET_MINUS_ONE(num_stack.empty())
        double b = num_stack.top();
        num_stack.pop();
        type_stack.pop();

        num_stack.push(b * a);
        type_stack.push(DOUBLE);

        break; 
    }
    case LexEnd:
    case 0:
        return 0;
    case LexUnknown:
    case LexParseError:
        // flag reset
        inside_array = false;
        array_last_num = false;
        return -1;
    }
    return 1;
}

string print_array(array v)
{
    ostringstream stream;
    auto sz = v.size();
    stream << "array(" << sz << ", ";
    for (auto i = 0; i < sz-1; i++) 
    {
        stream << v[i] << ", ";
    }
    stream << v[sz-1] << ")";
    return stream.str();
}

int calc_line()
{
    int token = yylex();
    if (token == 0) 
    {
        return 1;
    }

    for (;;) 
    {
        int cmd_res = process_command(token);
        if (cmd_res == 0) 
        {
            break;
        }
        else if (cmd_res == -1) 
        {
            cout << "Syntax error!" << endl;
            return 0;
        }
        token = yylex();
    }

    if (num_stack.empty() && arr_stack.empty()) 
    {
        debug(cout << "Both stacks are empty but a value is required!" << endl)
        return 0;
    }
    if (!num_stack.empty()) 
    {
        double result = num_stack.top();
        num_stack.pop();

        cout << result << " ";
    } else if (!arr_stack.empty()) 
    {
        array result = arr_stack.top();
        arr_stack.pop();
        cout << print_array(result) << " ";
    }
    if (!num_stack.empty() || !arr_stack.empty())
    {
        debug(cout << "Stacks are not empty after calculation!" << endl)
        return 0;
    }
    return 1;
}

void calc()
{
    while (!end_file) 
    {
        if (!is_testing) 
        {
            cout << "Enter expression: " << endl << "> ";
        }
        if (calc_line() == 0) 
        {
            cout << "FAIL" << endl;
            BEGIN(SKIPERROR);
            yylex();
            BEGIN(INITIAL);
        }
        else 
        {
            cout << "OK" << endl;
        }
        debug(cout << "line parsed" << endl)
    }
}

int main(int argc, char** argv)
{
	// debug flags
    is_testing = true;
    is_debug = true;
			
    end_file = false;
    calc();
    return 0;
}

int yywrap()
{
    end_file = true;
    return 1;
}